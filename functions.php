<?php
/**
 * aim functions and definitions
 *
 * @package aim
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
$content_width = 640; /* pixels */

if ( ! function_exists( 'aim_setup' ) ) :
                        /**
                         * Sets up theme defaults and registers support for various WordPress features.
                         *
                         * Note that this function is hooked into the after_setup_theme hook, which runs
                         * before the init hook. The init hook is too late for some features, such as indicating
                         * support post thumbnails.
                         */
                        function aim_setup() {


  /**
   *Define constant
   */
  define( 'HOME_URI', home_url() );
  define( 'THEME_URI', get_stylesheet_directory_uri() );
  define( 'LIBRARY', THEME_URI . '/library' );
  define( 'IMAGES', LIBRARY . '/images' );
  define( 'CSS', LIBRARY . '/css' );
  define( 'JS', LIBRARY . '/js' );
  define( 'PLUGINS', LIBRARY . '/plugins' );
  define( 'BOOTSTRAP', LIBRARY . '/bootstrap-3.0.0');

  /**
   * Make theme available for translation
   * Translations can be filed in the /languages/ directory
   * If you're building a theme based on aim, use a find and replace
   * to change 'aim' to the name of your theme in all the template files
   */
  load_theme_textdomain( 'aim', get_template_directory() . '/languages' );

  /**
   * Add default posts and comments RSS feed links to head
   */
  add_theme_support( 'automatic-feed-links' );

  /**
   * Enable support for Post Thumbnails on posts and pages
   *
   * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  //add_theme_support( 'post-thumbnails' );

  /**
   * This theme uses wp_nav_menu() in one location.
   */
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'aim' ),
  ) );

  /**
   * Enable support for Post Formats
   */
  add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

  /**
   * Setup the WordPress core custom background feature.
   */
  add_theme_support( 'custom-background', apply_filters( 'aim_custom_background_args', array(
    'default-color' => 'ffffff',
    'default-image' => '',
  ) ) );
}
endif; // aim_setup
add_action( 'after_setup_theme', 'aim_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function aim_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Sidebar', 'aim' ),
    'id'            => 'sidebar-1',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );
}
add_action( 'widgets_init', 'aim_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function aim_scripts() {
  wp_enqueue_style( 'aim-bootstrap-css', BOOTSTRAP . '/dist/css/bootstrap.min.css' );
  wp_enqueue_style( 'aim-style', get_stylesheet_uri() );

  // Include the class (unless you are using the script as a plugin)
  require_once( 'inc/wp-less/wp-less.php' );
  // enqueue a .less style sheet
  wp_enqueue_style( 'less-style', BOOTSTRAP . '/less/aim-style.less' );

  wp_enqueue_script( 'aim-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

  wp_enqueue_script( 'aim-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }

  if ( is_singular() && wp_attachment_is_image() ) {
    wp_enqueue_script( 'aim-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );

    wp_enqueue_script( 'aim-bootstrap-js', BOOTSTRAP . '/dist/js/bootstrap.min.js', array( 'jquery' ), '3.0.0' );
  }
}
add_action( 'wp_enqueue_scripts', 'aim_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
